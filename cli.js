#! /usr/bin/env node

import { exec } from 'child_process';
import { cpSync, existsSync, mkdirSync, writeFileSync } from 'fs';
import { dirname, join, sep } from 'path';
import { fileURLToPath } from 'url';
// 第三方库
import chalk from 'chalk';
import { Command } from 'commander';
import ejs from 'ejs';
import ora from 'ora';
import prompts from 'prompts';

(async () => {
  // process.cwd() 对应控制台所在目录
  const cwdUrl = process.cwd();
  // 此项目根目录
  const dir = dirname(fileURLToPath(import.meta.url));
  // 生成文件目录
  const configPath = join(cwdUrl, '.vscode');
  // 安装依赖目录
  const installPath = join(dir, 'src');
  // 模版文件目录;
  const templatePath = join(dir, 'src/templates');

  const program = new Command();
  program.name('vscode-lint').description('代码规范的终极解决方案');
  program
    .command('i')
    .description('初始化')
    .action(async () => {
      const pm = await prompts({
        type: 'select',
        name: 'val',
        message: '请选择初始化的包管理器',
        choices: [
          {
            title: '使用 npm',
            value: 'npm',
          },
          {
            title: '使用 yarn',
            value: 'yarn',
          },
          {
            title: '使用 pnpm',
            value: 'pnpm',
          },
        ],
        hint: '上下切换选项，回车以确认',
      });
      if (pm.val) {
        const spinner = ora('初始化中...').start();
        exec(
          `${pm.val} install`,
          {
            cwd: installPath,
          },
          error => {
            if (error) {
              spinner.fail('初始化失败');
              return;
            }
            spinner.succeed('初始化成功～');
          },
        );
      }
    });
  program
    .command('g')
    .description('生成配置文件')
    .action(async () => {
      const answers = await prompts({
        type: 'select',
        name: 'val',
        message: '请选择 ESLint 配置文件',
        choices: [
          {
            title: 'js',
            value: 'js',
            description: '适用于原生js项目',
          },
          {
            title: 'ts',
            value: 'ts',
            description: '适用于纯ts项目',
          },
          {
            title: 'vue2 + js',
            value: 'vue',
            description: '适用于vue2项目',
          },
          {
            title: 'vue3 + js',
            value: 'vue3',
            description: '适用于vue3项目',
          },
          {
            title: 'vue3 + ts',
            value: 'vue3-ts',
            description: '适用于vue3 + typescript项目',
          },
          {
            title: 'react + js',
            value: 'react',
            description: '适用于react项目',
          },
          {
            title: 'react + ts',
            value: 'react-ts',
            description: '适用于react + typescript项目',
          },
          {
            title: 'react-native + ts',
            value: 'react-native-ts',
            description: '适用于react-native + typescript项目',
          },
        ],
        hint: '上下切换选项，回车以确认',
      });
      if (answers.val) {
        // 从模版目录中读取文件
        let bool = true;
        if (existsSync(configPath)) {
          const confirm = await prompts({
            type: 'confirm',
            name: 'yes',
            message: '已存在配置文件，是否继续替换？',
            initial: true,
          });
          bool = confirm.yes;
        }
        if (bool) {
          mkdirSync(configPath, { recursive: true });
          ejs
            .renderFile(join(templatePath, 'settings.json'), {
              path: join(dir, 'src'),
              type: answers.val,
              sep: sep === '/' ? sep : '\\',
            })
            .then(data => {
              writeFileSync(join(configPath, 'settings.json'), data);
            });
          cpSync(join(templatePath, 'extensions.json'), join(configPath, 'extensions.json'));
          console.log('生成配置文件成功 ~');
          console.log(`配置文件路径: ${chalk.cyan('.vscode/settings.json')}`);
          console.log('请重启 vscode 以使配置生效 ~');
        }
      }
    });
  program.parse();
})();
