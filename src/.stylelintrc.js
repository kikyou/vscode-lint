module.exports = {
  extends: ['stylelint-config-idiomatic-order'],
  overrides: [
    {
      files: ['/**/*.scss'],
      customSyntax: 'postcss-scss',
    },
    {
      files: ['/**/*.less'],
      customSyntax: 'postcss-less',
    },
    {
      files: ['/**/*.vue'],
      customSyntax: 'postcss-html',
    },
  ],
};
