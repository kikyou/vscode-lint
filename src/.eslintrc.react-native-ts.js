module.exports = {
  extends: ['@react-native-community', './.eslintrc.base.js'],
  parser: '@typescript-eslint/parser',
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        'no-shadow': 0,
        'no-undef': 0,
      },
    },
  ],
};
