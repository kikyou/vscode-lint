module.exports = {
  extends: ['airbnb', 'airbnb-typescript', './.eslintrc.base.js'],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/naming-convention': 0,
    '@typescript-eslint/no-shadow': 0,
    '@typescript-eslint/no-redeclare': 0,
  },
};
