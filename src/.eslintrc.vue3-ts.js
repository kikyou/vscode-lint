module.exports = {
  extends: ['plugin:vue/vue3-recommended', './.eslintrc.ts.js'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    project: './tsconfig.json',
    extraFileExtensions: ['.vue'],
    vueFeatures: {
      styleCSSVariableInjection: true,
    },
  },
  rules: {},
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'no-undef': 0,
        'vue/html-self-closing': [
          2,
          {
            html: {
              void: 'always',
              normal: 'always',
              component: 'always',
            },
          },
        ],
        'vue/singleline-html-element-content-newline': 0,
        'vue/max-attributes-per-line': 0,
        'vue/no-setup-props-destructure': 0,
        'vue/multi-word-component-names': 0,
      },
    },
  ],
};
