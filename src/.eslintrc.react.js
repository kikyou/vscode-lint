module.exports = {
  extends: ['airbnb', 'airbnb/hooks', 'plugin:react/jsx-runtime', './.eslintrc.base.js'],
  rules: {
    'react/function-component-definition': [2, { namedComponents: 'arrow-function' }],
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  globals: {
    createRef: true,
    forwardRef: true,
    lazy: true,
    memo: true,
    startTransition: true,
    useCallback: true,
    useContext: true,
    useDebugValue: true,
    useDeferredValue: true,
    useEffect: true,
    useId: true,
    useImperativeHandle: true,
    useInsertionEffect: true,
    useLayoutEffect: true,
    useMemo: true,
    useReducer: true,
    useRef: true,
    useState: true,
    useSyncExternalStore: true,
    useTransition: true,
  },
};
