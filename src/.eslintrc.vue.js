module.exports = {
  extends: ['airbnb-base', 'plugin:vue/recommended', './.eslintrc.base.js'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@babel/eslint-parser',
  },
  rules: {},
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'vue/html-self-closing': [
          2,
          {
            html: {
              void: 'always',
              normal: 'always',
              component: 'always',
            },
          },
        ],
        'vue/singleline-html-element-content-newline': 0,
        'vue/max-attributes-per-line': 0,
        'vue/no-setup-props-destructure': 0,
        'vue/multi-word-component-names': 0,
      },
    },
  ],
};
