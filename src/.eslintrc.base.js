module.exports = {
  root: true,
  extends: ['prettier'],
  plugins: ['prettier'],
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: 'module',
  },
  rules: {
    // 允许使用console
    'no-console': 0,
    'arrow-body-style': [2, 'as-needed'],
    // 允许i++
    'no-plusplus': 0,
    // 允许在循环里使用await
    'no-await-in-loop': 0,
    // promise reject 可以返回任意值
    'prefer-promise-reject-errors': 0,
    // 允许匿名函数
    'func-names': 0,
    'no-param-reassign': [2, { props: false }],
    // 解构规则
    'prefer-destructuring': [
      2,
      {
        array: false,
        object: true,
      },
      {
        enforceForRenamedProperties: false,
      },
    ],
    // for in 检测
    'guard-for-in': 0,
    'no-shadow': 0,
    // 链式调用换行
    'newline-per-chained-call': 0,
    // 允许使用_前缀
    'no-underscore-dangle': 0,
    // import时永远省略后缀
    'import/extensions': [
      2,
      { js: 'never', ts: 'ignore', vue: 'always', jsx: 'always', tsx: 'always' },
    ],
    'import/no-extraneous-dependencies': 0,
    // 'import/no-unresolved': 0,
    // prettier配置
    'prettier/prettier': [
      2,
      {
        singleQuote: true,
        printWidth: 100,
        tabWidth: 2,
        semi: true,
        quoteProps: 'consistent',
        jsxSingleQuote: false,
        trailingComma: 'all',
        bracketSpacing: true,
        arrowParens: 'avoid',
        vueIndentScriptAndStyle: true,
        endOfLine: 'lf',
      },
      {
        usePrettierrc: false,
      },
    ],
  },
};
